/* 原始代码
import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    }
  ]
})
*/

import Vue from 'vue'
import Router from 'vue-router'

// 首页
const IndexPage = resolve => require(['@/pages/IndexPage'], resolve)
// 登陆
const LoginPage = resolve => require(['@/pages/LoginPage'], resolve)
// 奖品
const PrizePage = resolve => require(['@/pages/PrizePage'], resolve)
// 攻略
const GuidePage = resolve => require(['@/pages/GuidePage'], resolve)
// 用户
const HomePage = resolve => require(['@/pages/HomePage'], resolve)
// 错误
const ErrorPage = resolve => require(['@/pages/ErrorPage'], resolve)

Vue.use(Router)
export default new Router({
  routes: [
    {
      path: '*',
      redirect: '/'
    },
    {
      path: '/',
      name: '首页',
      component: IndexPage,
      meta: {
        keepAlive: true
      }
    },
    {
      path: '/login',
      name: '登陆',
      component: LoginPage,
      meta: {
        keepAlive: false
      }
    },
    {
      path: '/prize',
      name: '奖品',
      component: PrizePage,
      meta: {
        keepAlive: true
      }
    },
    {
      path: '/guide',
      name: '攻略',
      component: GuidePage,
      meta: {
        keepAlive: true
      }
    },
    {
      path: '/home',
      name: '用户',
      component: HomePage,
      meta: {
        keepAlive: true
      }
    },
    {
      path: '/error',
      name: '错误',
      component: ErrorPage,
      meta: {
        keepAlive: true
      }
    }
  ]
})

// 微信公共函数
import { cookie } from 'vux'
export default {
  /**
   * 判断是否为微信客户端
   * @returns {boolean}
   */
  isClient: function () {
    const ua = window.navigator.userAgent.toLowerCase()
    if (ua.match(/MicroMessenger/i) == 'micromessenger') { //eslint-disable-line
      return true
    }
    return false
  },
  /**
   * 获取认证token
   * @param id
   * @param field
   * @returns {null}
   */
  getToken: function (id, field) {
    if (!id) {
      return null
    }
    field = field || 'wx_token'
    field = field + '_' + id
    return cookie.get(field)
  }
}

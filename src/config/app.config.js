export default {
  // 公众号账号
  wx_official: 3,
  main_id: 2,
  // base_url: 'http://wx.0817ch.com/',
  // 参与接口
  // api_actor_add: 'api/koi/actor/add'
  api_main_show: 'http://wx.0817ch.com/koi_yehoo/main/show/id/2',
  api_main_share: 'http://wx.0817ch.com/koi_yehoo/main/share/id/2',
  api_actor_show: 'http://wx.0817ch.com/koi_yehoo/actor/show/main/2',
  api_actor_add: 'http://wx.0817ch.com/koi_yehoo/actor/add/main/2',
  api_actor_list: 'http://wx.0817ch.com/koi_yehoo/actor/items/main/2',
  api_prize_list: 'http://wx.0817ch.com/koi_yehoo/prize/items/main/2',
  api_luck_list: 'http://wx.0817ch.com/koi_yehoo/luck/items/main/2',
  // 红包记录
  api_wxred_list: 'http://wx.0817ch.com/koi_yehoo/luck/wxred/main/2'
}

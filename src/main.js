/* 原始代码
import Vue from 'vue'
import FastClick from 'fastclick'
import VueRouter from 'vue-router'
import App from './App'
import Home from './components/HelloFromVux'

Vue.use(VueRouter)
const routes = [{
  path: '/',
  component: Home
}]

const router = new VueRouter({
  routes
})

FastClick.attach(document.body)

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app-box')
*/
import Vue from 'vue'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import FastClick from 'fastclick'
import {AjaxPlugin, WechatPlugin, ToastPlugin, LoadingPlugin, AlertPlugin, cookie} from 'vux'
import wxTool from './common/wx'
import appConfig from './config/app.config'
import App from './App'
import router from './router'

// 解决移动端点击延迟
FastClick.attach(document.body)
require('es6-promise').polyfill()
Vue.use(Vuex)
Vue.use(VueRouter)
Vue.use(AjaxPlugin)
Vue.use(WechatPlugin)
Vue.use(ToastPlugin)
Vue.use(LoadingPlugin)
Vue.use(AlertPlugin)

const store = new Vuex.Store({
  state: {
    wxShareTitle: '',
    wxShareDesc: '',
    wxShareLink: '',
    wxShareImage: ''
  },
  mutations: {
    setWxShare: function (state, payload) {
      state.wxShareTitle = payload.title
      state.wxShareDesc = payload.desc
      state.wxShareLink = payload.link
      state.wxShareImage = payload.image
    }
  }
})

Vue.config.productionTip = false
Vue.prototype.$wxTool = wxTool
Vue.prototype.$cookie = cookie
Vue.prototype.$appConfig = appConfig

// region 请求拦截
AjaxPlugin.$http.interceptors.request.use(
  config => {
    let wxToken = wxTool.getToken(appConfig.wx_official)
    if (config.data && wxToken) {
      /*
      config.data = Object.assign(config.data, {
        'auth_token': token
      })
      */
      config.data.wx_token = wxToken
    }
    // config.data = JSON.stringify(config.data)
    config.headers = {
      // 让后端识别这是一个ajax请求
      'X-Requested-With': 'XMLHttpRequest',
      // 'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8'
      'Content-Type': 'application/json'
    }
    return config
  },
  err => {
    return Promise.reject(err)
  }
)
// endregion

// region 路由拦截
// let routerArray = [] // 存储路由
router.beforeEach(function (to, from, next) {
  let token = null
  if (wxTool.isClient()) {
    // 微信防屏蔽
    // let url = window.location.href
    token = wxTool.getToken(appConfig.wx_official)
  } else { // 非微信浏览器跳转到错误页
    if (to.path !== '/error') {
      // 跳转到错误页
      next('/error')
      return false
    }
    next()
    return false
  }
  if (!token && to.path !== '/login') { // 没登陆
    next('/login')
    return false
  }
  next()
  return false
})
// endregion

// 微信SDK配置
if (wxTool.isClient()) {
  Vue.http.post(appConfig.api_main_share, {
    // 签名链接
    sign_link: window.location.href
  }).then((response) => {
    console.log(response)
    if (response.data.code === 1) {
      Vue.wechat.config(JSON.parse(response.data.data.config))
      Vue.wechat.ready(() => {
        store.commit('setWxShare', response.data.data)
        let wxShareTitle = response.data.data.title
        let wxShareDesc = response.data.data.desc
        let wxShareLink = response.data.data.link
        let wxShareImage = response.data.data.image
        // 分享给朋友
        Vue.wechat.onMenuShareAppMessage({ // 分享给朋友
          title: wxShareTitle, // 分享标题
          desc: wxShareDesc, // 分享描述
          link: wxShareLink, // 分享链接 默认以当前链接
          imgUrl: wxShareImage, // 分享图标
          // 用户确认分享后执行的回调函数
          success: function () {
            console.log('分享朋友成功')
          },
          // 用户取消分享后执行的回调函数
          cancel: function () {
            console.log('取消分享朋友')
          }
        })
        // 分享到朋友圈
        Vue.wechat.onMenuShareTimeline({
          title: wxShareTitle, // 分享标题
          desc: wxShareDesc, // 分享描述
          link: wxShareLink, // 分享链接 默认以当前链接
          imgUrl: wxShareImage, // 分享图标
          // 用户确认分享后执行的回调函数
          success: function () {
            console.log('分享朋友圈成功')
          },
          // 用户取消分享后执行的回调函数
          cancel: function () {
            console.log('取消分享朋友圈')
          }
        })
      })
    }
  }).catch(function (error) {
    console.log(error)
  })
}
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app-box')
